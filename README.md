<h1 align = "center">IEEE Conference Template</h1>

<b>Connect with me at:</b>
<a href = "https://www.linkedin.com/in/dpramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/linkedin.svg"/></a>
	<a href = "https://github.com/ZenithClown"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/github.svg"/></a>
	<a href = "https://gitlab.com/ZenithClown/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/gitlab.svg"/></a>
	<a href = "https://www.researchgate.net/profile/Debmalya_Pramanik2"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/researchgate.svg"/></a>
	<a href = "https://www.kaggle.com/dPramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/kaggle.svg"/></a>
	<a href = "https://app.pluralsight.com/profile/Debmalya-Pramanik/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/pluralsight.svg"/></a>
	<a href = "https://stackoverflow.com/users/6623589/"><img height="16" width="16" src="https://unpkg.com/simple-icons@v3/icons/stackoverflow.svg"/></a>

**Institute of Electrical and Electronics Engineers (IEEE)** is a technical professional orginizations dedicated to advancing technology for the benefit of humanity. Here, in this Repository, I present a simple-minimal <img src="https://latex.codecogs.com/gif.latex?\text { \LaTeX } " /> template for this Journal.

*You are free to do anything with this template, and change the same according to your need!*

## Project Release(s) & Details
IEEE has its [own source code, with Templates](https://www.ieee.org/conferences/publishing/templates.html), however I have just modified some of its section for easy use and understanding purposes only. Also, I have copied some portion from the [IEEE Bare Demo Template, Overleaf](https://www.overleaf.com/latex/templates/ieee-bare-demo-template-for-conferences/ypypvwjmvtdf) where required.
